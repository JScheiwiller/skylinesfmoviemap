const express = require('express');
const app = express();
const port = 3555;

const parse = require('csv-parse');
const Chance = require('chance');
var chance = new Chance();

const fs = require('fs');
const _ = require('lodash');

let records;

const DECIMALS = 6;

fs.readFile("Film_Locations_in_San_Francisco.csv", {encoding:"utf8"}, (errR, data) => {
    parse(data, {}, (errP, results) => {
        const keys = _.first(results).map(k => k.replace(/ /g, '')),
            dataArrs = _.tail(results);

        records = dataArrs.map((values, arri) => {
            console.log('Processing rec ind ' + arri);
    
            return keys.reduce((obj, k, i) => {
                obj[k] = values[i];

                const randLat = chance.floating({min: 37.649632, max: 37.784337, fixed: DECIMALS});
                const randLng = chance.floating({min: -122.508281, max: -122.381994, fixed: DECIMALS});

                obj.Latitude = randLat;
                obj.Longitude = randLng;
    
                return obj;
            }, {});
        });

        console.log('Finished processing');
    })
})

app.get('/sfMovies', (request, response) => {
    response.header('Access-Control-Allow-Origin', '*');
    response.send(records);
})

app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
})