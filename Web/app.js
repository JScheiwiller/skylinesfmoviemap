import React from 'react'
import ReactDOM from 'react-dom'
import Root from './components/Root'
import './styles.css'

ReactDOM.render(<Root />, document.getElementById('app'))