import React from 'react';

const {useState} = React;

const FilterPanel = ({search, reset, keyInfo}) => {
    const [filterState, setFilterState] = useState({});

    const updateFilterState = (e) => {
        const tgt = e.target;

        const newFilterState = {...filterState};

        newFilterState[tgt.name] = tgt.value;

        setFilterState(newFilterState)
    };

    const resetFp = () => {
        setFilterState({});
        reset();
    };

    return (
        <div style={{padding:'5px'}}>
            <table>
            <tbody>
            {keyInfo.map((ki, i) => (
                <tr key={i}>
                    <td>
                        <label>{`${ki.label || ki.key}: `}</label>
                    </td>
                    <td>
                        <input type='text' name={ki.key} value={filterState[ki.key] || ''} onChange={e => updateFilterState(e)}></input><br/>
                    </td>
                </tr>
            ))}
            </tbody>
            </table>
            <button style={{float:'right', marginTop: '5px'}} onClick={e => search(filterState)}>Search</button>
            <button style={{float:'right', marginTop: '5px', marginRight: '5px'}} onClick={e => resetFp()}>Reset</button>
        </div>
    );
}

export default FilterPanel