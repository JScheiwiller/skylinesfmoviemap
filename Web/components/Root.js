import React from 'react';
import Map from './Map';
import FilterPanel from './FilterPanel';
import config from '../config';

const {useState, useEffect} = React;
const {headers} = config;

const Root = () => {
  const [records, setRecords] = useState([]);
  const [filteredRecords, setFilteredRecords] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch('http://localhost:3555/sfMovies', headers);

      response.json().then(data => {
        setRecords(data);
        setFilteredRecords(data);
      });
    };

    fetchData();
  }, []);

  const search = filterState => {
    const activeFilters = Object.keys(filterState).reduce((fs, k) => {
      const val = filterState[k];

      if(val){
        fs[k] = val;
      }

      return fs;
    }, {});

    const filteredSet = records.filter(r => Object.keys(activeFilters).every(k => {
      const recordVal = r[k];

      return recordVal && recordVal.toLowerCase().includes(activeFilters[k].toLowerCase())
    }));

    setFilteredRecords(filteredSet)
  };

  const reset = () => {
    setFilteredRecords(records);
  };

  const keyInfo = [
    {key: 'Title'}, 
    {key: 'ReleaseYear', label: 'Release Year'},
    {key: 'Locations'}, 
    {key: 'FunFacts', label: 'Fun Facts'},
    {key: 'ProductionCompany', label: 'Production Company'},
    {key: 'Distributor'},
    {key: 'Director'},
    {key: 'Writer'},
    {key: 'Actor1'},
    {key: 'Actor2'},
    {key: 'Actor3'}
  ];

  return <div>
          <h1 className="front-title">San Francisco Movie Data Map</h1>
          <div className="container">
            <div>
              <FilterPanel search={search} reset={reset} keyInfo={keyInfo}></FilterPanel>
            </div>
            <div className="flex-grow-3">
              <Map records={filteredRecords} keyInfo={keyInfo}></Map>
            </div>
          </div>
        </div>
}
  
export default Root