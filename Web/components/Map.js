import React from 'react';
import ReactMapGL, {Marker, Popup} from 'react-map-gl';
import config from '../config';

const {useState} = React;
const {mapboxApiAccessToken} = config;

const Map = ({records, keyInfo}) => {
  //Show only 100 at a time
  const reducedRecords = records.slice(0, 100)
                                .map(r => 
                                  Object.keys(r).reduce((obj, k) => {
                                    obj[k] = r[k] || 'None';
                                    return obj;
                                  }, {}));

  const [state, setState] = useState({
    viewport: {
      width: '100%',
      height: 1000,
      latitude: 37.7577,
      longitude: -122.4376,
      zoom: 8
    }
  });

  const [selectedItem, setSelectedItem] = useState(null);

  const onViewportChange = viewport => {
    setState({viewport});
  }
  
  return (
    <ReactMapGL mapboxApiAccessToken={mapboxApiAccessToken}
      mapStyle={'mapbox://styles/jscheiwiller/ck3dgwmne2yf91cmk8zxw35u4'}
      {...state.viewport}
      onViewportChange={onViewportChange}
    >
      {reducedRecords.map((r, i) => (
        <Marker
          key={i}
          latitude={r.Latitude}
          longitude={r.Longitude}
          >
            <button onClick={e => {
              e.preventDefault();
              setSelectedItem(r);
            }}>
              <img src='/video.svg' alt='Movie location' style={{width:"20px",height:"20px"}}/>
            </button>
        </Marker>
      ))}

      {selectedItem && 
        <Popup
          latitude={selectedItem.Latitude}
          longitude={selectedItem.Longitude}
          onClose={() => {setSelectedItem(null);}}>
            <div>
              <h2>{selectedItem.Title}</h2>
              {keyInfo.map((ki, i) => (<p key={i}>{`${ki.label || ki.key}: ${selectedItem[ki.key]}`}</p>))}
            </div>
        </Popup>
      }
    </ReactMapGL>
  );
}

export default Map