export default {
    headers: { 
        // 'X-App-Token': 'x3b3OkMiZFjR9k0R5qleIjvn1',
        'Access-Control-Allow-Origin:': '*'
    },
    mapboxApiAccessToken: 'pk.eyJ1IjoianNjaGVpd2lsbGVyIiwiYSI6ImNrM2RndjUzOTE2bnUzY3FsamVlNjIzOTcifQ.Z0yIMs3zoPRKy9WvjftL7g'
}