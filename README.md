San Francisco Movie Data Map

Summary: 
Shows artificial locations of movies filmed in San Francisco, limited to 100 items at a time.
Allows filtering of results.
(Location names could not easily be converted to coordinates, so randomly generated coordinates are used)

*Requires Node.js*

Development: 
Server:
    *  Run npm install in /Server dir
    *  Run node server.js in /Server dir

	
Web:
    * Run npm install in /Web dir
    * Run npm start in /Web dir
    * Navigate to localhost:8080 in browser

	
Production:
Server:
    *  Host as regular Node app 
	
Web:
    * Build bundle with npm run build in /Web dir
    * Host artifacts in /Web/dist with web server
